package tn.esprit.pidev.fdz.administrator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import tn.esprit.pidev.fdz.administrator.util.delegates.UserDelegate;
import tn.esprit.pidev.fdz.entities.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class LockUser extends JPanel {
	private JTable table;
	List<User> users=new ArrayList<>();
	User user = new User();

	/**
	 * Create the panel.
	 */
	public LockUser(List<User> lu) {
		setBackground(new Color(240, 255, 255));
		
		users=lu;
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnLockunlock = new JButton("Lock/Unlock");
		btnLockunlock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int x =table.getSelectedRow();
				if(x==-1)
				{JOptionPane.showMessageDialog(null, "you must select a row");}
				else{
				user=users.get(x);
				
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"+x);
				 
				if (user.getStatus().equalsIgnoreCase("unlocked")) 
					
					
				 
			    {
					user.setStatus("locked");
				    UserDelegate.edit(user);
				   
				
			
				}
				else if(user.getStatus().equalsIgnoreCase("locked"))
				{
					user.setStatus("unlocked");
					UserDelegate.edit(user);
			
				}
				
				
			users=UserDelegate.findAll();
			initDataBindings();
				//clear();
			
			}
			}
		});
		
		JLabel lblUsersList = new JLabel("User's List");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(170)
							.addComponent(btnLockunlock))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(49)
							.addComponent(lblUsersList))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(64, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(lblUsersList)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
					.addGap(44)
					.addComponent(btnLockunlock)
					.addContainerGap(59, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setBackground(new Color(51, 204, 255));
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				user=users.get(table.getSelectedRow());}
		});
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		initDataBindings();

	}
	
	protected void initDataBindings() {
		JTableBinding<User, List<User>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, users, table);
		//
		BeanProperty<User, String> UserBeanProperty = BeanProperty.create("firstName");
		jTableBinding.addColumnBinding(UserBeanProperty).setColumnName("firstName");
		//
		BeanProperty<User, String> UserBeanProperty_1 = BeanProperty.create("lastName");
		jTableBinding.addColumnBinding(UserBeanProperty_1).setColumnName("lastName");
		//
		BeanProperty<User, String> UserBeanProperty_2 = BeanProperty.create("login");
		jTableBinding.addColumnBinding(UserBeanProperty_2).setColumnName("login");
		//
		BeanProperty<User, String> UserBeanProperty_3 = BeanProperty.create("status");
		jTableBinding.addColumnBinding(UserBeanProperty_3).setColumnName("status");
		//
		BeanProperty<User, String> UserBeanProperty_4 = BeanProperty.create("city");
		jTableBinding.addColumnBinding(UserBeanProperty_4).setColumnName("city");
		//
		BeanProperty<User, String> UserBeanProperty_5 = BeanProperty.create("country");
		jTableBinding.addColumnBinding(UserBeanProperty_5).setColumnName("country");
		//
		BeanProperty<User, String> UserBeanProperty_6 = BeanProperty.create("email");
		jTableBinding.addColumnBinding(UserBeanProperty_6).setColumnName("email");
		
		jTableBinding.bind();
	}
	
}
