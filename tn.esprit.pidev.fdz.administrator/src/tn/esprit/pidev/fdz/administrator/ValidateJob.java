package tn.esprit.pidev.fdz.administrator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import tn.esprit.pidev.fdz.administrator.util.delegates.JobDelegate;
import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.entities.JobOwner;
import tn.esprit.pidev.fdz.entities.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class ValidateJob extends JPanel {
	private JTable table;
	List<Job> jobs=new ArrayList<>();
	Job job = new Job();
	JobOwner jo= new JobOwner();

	/**
	 * Create the panel.
	 */
	public ValidateJob(List<Job> jb) {
		setBackground(new Color(240, 240, 240));
		
		jobs=jb;
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblJobsList = new JLabel("Job's List");
		
		JButton btnAccept = new JButton("Accept");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int x =table.getSelectedRow();
				if(x==-1)
				{JOptionPane.showMessageDialog(null, "you must select a row");}
				else{
				job=jobs.get(x);
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"+x);
				 
				if (job.getEtat()==0) 
	
			    {
					job.setEtat(1);
				   JobDelegate.edit(job);

				}
			jobs=JobDelegate.findByEtat(0);
			initDataBindings();
							
			}
				
			}
		});
		
		JButton btnReject = new JButton("Reject");
		btnReject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int x =table.getSelectedRow();
				if(x==-1)
				{JOptionPane.showMessageDialog(null, "you must select a row");}
				else{
				job=jobs.get(x);
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"+x);	
				
				
				   JobDelegate.remove(job);
				  
				
			jobs=JobDelegate.findByEtat(0);
			initDataBindings();
				
			 jo = (JobOwner)job.getJobOwner();
			System.out.println(jo.getEmail()+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			SendMail sm = new SendMail(jo);
		sm.setVisible(true);
//			SendMail sm = new SendMail();
//			sm.setVisible(true);
			}
				
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(49)
							.addComponent(lblJobsList))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(31)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(101)
							.addComponent(btnAccept)
							.addGap(78)
							.addComponent(btnReject)))
					.addContainerGap(64, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(lblJobsList)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnReject)
						.addComponent(btnAccept))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setBackground(Color.LIGHT_GRAY);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				job=jobs.get(table.getSelectedRow());}
		});
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
		initDataBindings();

	}
	
	protected void initDataBindings() {
		JTableBinding<Job, List<Job>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, jobs, table);
		//
		BeanProperty<Job, String> jobBeanProperty = BeanProperty.create("attachement");
		jTableBinding.addColumnBinding(jobBeanProperty).setColumnName("attachement");
		//
		BeanProperty<Job, String> jobBeanProperty_1 = BeanProperty.create("description");
		jTableBinding.addColumnBinding(jobBeanProperty_1).setColumnName("description");
		//
		BeanProperty<Job, String> jobBeanProperty_2 = BeanProperty.create("duration");
		jTableBinding.addColumnBinding(jobBeanProperty_2).setColumnName("duration");
		//
		BeanProperty<Job, String> jobBeanProperty_3 = BeanProperty.create("etat");
		jTableBinding.addColumnBinding(jobBeanProperty_3).setColumnName("etat");
		//
		BeanProperty<Job, String> jobBeanProperty_4 = BeanProperty.create("workLoad");
		jTableBinding.addColumnBinding(jobBeanProperty_4).setColumnName("workLoad");
		//
		
		BeanProperty<Job, String> jobBeanProperty_5 = BeanProperty.create("jobOwner");
		jTableBinding.addColumnBinding(jobBeanProperty_5).setColumnName("jobOwner_id");
		
		
	
	
		jTableBinding.bind();
	}
	
}
