package tn.esprit.pidev.fdz.administrator;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;



import tn.esprit.pidev.fdz.administrator.util.delegates.UserDelegate;
import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.entities.JobOwner;
import tn.esprit.pidev.fdz.entities.User;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;
public class Gestionuser extends JFrame {

	private JPanel contentPane;
List<User> users=new ArrayList<>();
User user = new User();
private JTable table;
private JTextField Firstname;
private JTextField lastName;
private JTextField Login;
private JTextField Status;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gestionuser frame = new Gestionuser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gestionuser() {
		
		users=UserDelegate.findAll();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,650, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Users List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(40, 20, 424, 163);
		contentPane.add(panel);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNewButton1 = new JButton("Lock/Unlock");
		
		//JButton btnNewButton2 = new JButton("Unlock");
		btnNewButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int x =table.getSelectedRow();
				if(x==-1)
				{JOptionPane.showMessageDialog(null, "you must select a row");}
				else{
				user=users.get(x);
				
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"+x);
				 
				if (user.getStatus().equalsIgnoreCase("unlocked")) 
					
					
				 
			    {
					user.setStatus("locked");
				    UserDelegate.edit(user);
				   
				
			
				}
				else if(user.getStatus().equalsIgnoreCase("locked"))
				{
					user.setStatus("unlocked");
					UserDelegate.edit(user);
			
				}
				
				
			users=UserDelegate.findAll();
			initDataBindings();
				//clear();
			
			}}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(btnNewButton1, Alignment.TRAILING))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
					.addComponent(btnNewButton1))
		);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				user=users.get(table.getSelectedRow());

//Firstname.setText(user.getFirstName());
//lastName.setText(user.getLastName());
//Login.setText(user.getLogin());
			}
		});
		scrollPane.setViewportView(table);
		panel.setLayout(gl_panel);
		
//	JLabel lblFirstName = new JLabel("FirstName");
//	lblFirstName.setBounds(50, 185, 46, 14);
//	contentPane.add(lblFirstName);
//	
//		Firstname = new JTextField();
//		Firstname.setBounds(91, 185, 172, 20);
//		contentPane.add(Firstname);
//	Firstname.setColumns(10);
//	
//			JLabel lblLastName = new JLabel("LastName");
//	lblLastName.setBounds(30, 220, 46, 14);
//	contentPane.add(lblLastName);
//	
//	lastName = new JTextField();
//	lastName.setBounds(91, 217, 172, 20);
//	
//	contentPane.add(lastName);
//		lastName.setColumns(10);
//	
//	JLabel lblLogin = new JLabel("Login");
//	lblLogin.setBounds(20, 250, 46, 14);
//	contentPane.add(lblLogin);
//	
//	Login = new JTextField();
//	Login.setBounds(91, 320, 172, 20);
//	contentPane.add(Login);
//	Login.setColumns(10);
//		
//	
//	JButton btnUpdate = new JButton("update");
//	btnUpdate.addActionListener(new ActionListener() {
//		public void actionPerformed(ActionEvent e) {
//			user.setFirstName(Firstname.getText());
//	user.setLastName(lastName.getText());
//		UserDelegate.edit(user);
//		
//			clear();
//		users=UserDelegate.findAll();
//			initDataBindings();
//	}
//
//	
//	}
		
		
//	btnUpdate.setBounds(287, 202, 89, 23);
//		contentPane.add(btnUpdate);
		initDataBindings();
	}
//	public void clear() {
//	Firstname.setText("");
//		lastName.setText("");
//		Login.setText("");
//		
//}
	protected void initDataBindings() {
		JTableBinding<User, List<User>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, users, table);
		//
		BeanProperty<User, String> UserBeanProperty = BeanProperty.create("firstName");
		jTableBinding.addColumnBinding(UserBeanProperty).setColumnName("firstName");
		//
		BeanProperty<User, String> UserBeanProperty_1 = BeanProperty.create("lastName");
		jTableBinding.addColumnBinding(UserBeanProperty_1).setColumnName("lastName");
		//
		BeanProperty<User, String> UserBeanProperty_2 = BeanProperty.create("login");
		jTableBinding.addColumnBinding(UserBeanProperty_2).setColumnName("login");
		//
		BeanProperty<User, String> UserBeanProperty_3 = BeanProperty.create("status");
		jTableBinding.addColumnBinding(UserBeanProperty_3).setColumnName("status");
		//
		BeanProperty<User, String> UserBeanProperty_4 = BeanProperty.create("city");
		jTableBinding.addColumnBinding(UserBeanProperty_4).setColumnName("city");
		//
		BeanProperty<User, String> UserBeanProperty_5 = BeanProperty.create("country");
		jTableBinding.addColumnBinding(UserBeanProperty_5).setColumnName("country");
		//
		BeanProperty<User, String> UserBeanProperty_6 = BeanProperty.create("email");
		jTableBinding.addColumnBinding(UserBeanProperty_6).setColumnName("email");
		
		jTableBinding.bind();
	}
}


