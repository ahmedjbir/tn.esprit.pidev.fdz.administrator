package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.AddRequest;
import tn.esprit.pidev.fdz.services.interfaces.AddRequestCrudRemote;

public class AddRequestDelegate {
	
private static AddRequestCrudRemote proxy = (AddRequestCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/AddRequestCrud!tn.esprit.pidev.fdz.services.interfaces.AddRequestCrudRemote");
	
	
	
	public static void create(AddRequest addRequest)
	{
		proxy.create(addRequest);
	}
	public static void edit(AddRequest addRequest)
	{
		proxy.edit(addRequest);
	}
	public static void remove(AddRequest addRequest)
	{
		proxy.remove(addRequest);
	}
	public static AddRequest find(int id)
	{
		return proxy.find(id);
	}
	public  static List<AddRequest> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}

}
