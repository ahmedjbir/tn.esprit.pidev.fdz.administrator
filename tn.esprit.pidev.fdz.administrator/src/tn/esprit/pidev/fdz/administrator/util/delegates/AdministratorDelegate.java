package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Administrator;
import tn.esprit.pidev.fdz.services.interfaces.AdministratorCrudRemote;

public class AdministratorDelegate {
	
	private static AdministratorCrudRemote proxy = (AdministratorCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/AdministratorCrud!tn.esprit.pidev.fdz.services.interfaces.AdministratorCrudRemote");
	
	
	
	public static void create(Administrator administrator)
	{
		proxy.create(administrator);
	}
	public static void edit(Administrator administrator)
	{
		proxy.edit(administrator);
	}
	public static void remove(Administrator administrator)
	{
		proxy.remove(administrator);
	}
	public static Administrator find(int id)
	{
		return proxy.find(id);
	}
	public static List<Administrator> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}
	public static Administrator findByLoginAndPassword(String login, String password)
	{
		return proxy.findByLoginAndPassword(login, password);
	}
	

}
