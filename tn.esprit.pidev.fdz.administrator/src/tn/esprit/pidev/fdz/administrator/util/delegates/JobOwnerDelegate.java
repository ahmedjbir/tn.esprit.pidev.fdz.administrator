package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.JobOwner;
import tn.esprit.pidev.fdz.services.interfaces.JobOwnerCrudRemote;

public class JobOwnerDelegate {
	
private static JobOwnerCrudRemote proxy = (JobOwnerCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/JobOwnerCrud!tn.esprit.pidev.fdz.services.interfaces.JobOwnerCrudRemote");
	
	
	
	public static void create(JobOwner jobOwner)
	{
		proxy.create(jobOwner);
	}
	public static void edit(JobOwner jobOwner)
	{
		proxy.edit(jobOwner);
	}
	public static void remove(JobOwner jobOwner)
	{
		proxy.remove(jobOwner);
	}
	public static JobOwner find(int id)
	{
		return proxy.find(id);
	}
	public static List<JobOwner> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}


}
