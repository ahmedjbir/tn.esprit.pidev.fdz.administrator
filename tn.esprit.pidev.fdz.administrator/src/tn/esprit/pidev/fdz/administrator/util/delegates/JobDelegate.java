package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudRemote;

public class JobDelegate {
	
private static JobCrudRemote proxy = (JobCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/JobCrud!tn.esprit.pidev.fdz.services.interfaces.JobCrudRemote");
	
	
	
	public static void create(Job job)
	{
		proxy.create(job);
	}
	public static void edit(Job job)
	{
		proxy.edit(job);
	}
	public static void remove(Job job)
	{
		proxy.remove(job);
	}
	public static Job find(int id)
	{
		return proxy.find(id);
	}
	public static List<Job> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}
	public static List<Object[]> groupByCategory()
	{
		return proxy.groupByCategory();
	}
	public static List<Object[]> groupByRate()
	{
		return proxy.groupByRate();
	}
	public static List<Job> findByEtat(int etat)
	{
		return proxy.findByEtat(etat);
	}


}
