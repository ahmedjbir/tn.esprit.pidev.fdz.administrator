package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Sharing;
import tn.esprit.pidev.fdz.services.interfaces.SharingCrudRemote;

public class SharingDelegate {
	
private static SharingCrudRemote proxy = (SharingCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/SharingCrud!tn.esprit.pidev.fdz.services.interfaces.SharingCrudRemote");
	
	
	
	public static void create(Sharing sharing)
	{
		proxy.create(sharing);
	}
	public static void edit(Sharing sharing)
	{
		proxy.edit(sharing);
	}
	public static void remove(Sharing sharing)
	{
		proxy.remove(sharing);
	}
	public static Sharing find(int id)
	{
		return proxy.find(id);
	}
	public static List<Sharing> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}


}
