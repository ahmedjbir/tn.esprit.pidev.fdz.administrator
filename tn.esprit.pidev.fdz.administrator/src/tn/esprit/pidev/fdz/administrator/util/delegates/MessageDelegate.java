package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Message;
import tn.esprit.pidev.fdz.services.interfaces.MessageCrudRemote;

public class MessageDelegate {
	
private static MessageCrudRemote proxy = (MessageCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/MessageCrud!tn.esprit.pidev.fdz.services.interfaces.MessageCrudRemote");
	
	
	
	public static void create(Message message)
	{
		proxy.create(message);
	}
	public static void edit(Message message)
	{
		proxy.edit(message);
	}
	public static void remove(Message message)
	{
		proxy.remove(message);
	}
	public static Message find(int id)
	{
		return proxy.find(id);
	}
	public static List<Message> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}


}
