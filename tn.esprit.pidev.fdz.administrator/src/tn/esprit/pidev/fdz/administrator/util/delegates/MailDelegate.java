package tn.esprit.pidev.fdz.administrator.util.delegates;


import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.services.interfaces.MailRemote;

public class MailDelegate {
	
private static MailRemote proxy = (MailRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/Mail!tn.esprit.pidev.fdz.services.interfaces.MailRemote");
	
	
public static void sendMail(String toAddress, String subject, String content)
{
	proxy.sendMail(toAddress, subject, content);
}


}
