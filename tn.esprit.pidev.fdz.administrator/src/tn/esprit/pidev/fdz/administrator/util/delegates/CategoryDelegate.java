package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;




import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Category;
import tn.esprit.pidev.fdz.services.interfaces.CategoryCrudRemote;

public class CategoryDelegate {
	
private static CategoryCrudRemote proxy = (CategoryCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/CategoryCrud!tn.esprit.pidev.fdz.services.interfaces.CategoryCrudRemote");
	
	
	
	public static void create(Category category)
	{
		proxy.create(category);
	}
	public static void edit(Category category)
	{
		proxy.edit(category);
	}
	public static void remove(Category category)
	{
		proxy.remove(category);
	}
	public static Category find(int id)
	{
		return proxy.find(id);
	}
	public static List<Category> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}
	public static List<Category> ListCategory()
	{
		return proxy.ListCategory();
	}
	public static List<Category> ListSubCategory(Category idCategory)
	{
		return proxy.ListSubCategory(idCategory);
	}
	public static Category findByName(String name)
	{
		return proxy.findByName(name);
	}

}
