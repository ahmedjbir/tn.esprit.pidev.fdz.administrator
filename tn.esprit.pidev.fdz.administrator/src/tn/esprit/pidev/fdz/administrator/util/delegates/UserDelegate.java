package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.User;
import tn.esprit.pidev.fdz.services.interfaces.UserCrudRemote;

public class UserDelegate {
	
private static UserCrudRemote proxy = (UserCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/UserCrud!tn.esprit.pidev.fdz.services.interfaces.UserCrudRemote");
	
	
	
	public static void create(User user)
	{
		proxy.create(user);
	}
	public static void edit(User user)
	{
		proxy.edit(user);
	}
	public static void remove(User user)
	{
		proxy.remove(user);
	}
	public static User find(int id)
	{
		return proxy.find(id);
	}
	public static List<User> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}
	public static List<User> findByLogin(String login)
	{
		return proxy.findByLogin(login);
	}


}
