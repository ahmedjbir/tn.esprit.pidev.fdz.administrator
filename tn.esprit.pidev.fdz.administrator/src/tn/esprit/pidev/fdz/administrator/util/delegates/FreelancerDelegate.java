package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudRemote;

public class FreelancerDelegate {
	
private static FreelancerCrudRemote proxy = (FreelancerCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/FreelancerCrud!tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudRemote");
	
	
	
	public static void create(Freelancer freelancer)
	{
		proxy.create(freelancer);
	}
	public static void edit(Freelancer freelancer)
	{
		proxy.edit(freelancer);
	}
	public static void remove(Freelancer freelancer)
	{
		proxy.remove(freelancer);
	}
	public static Freelancer find(int id)
	{
		return proxy.find(id);
	}
	public static List<Freelancer> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}


}
