package tn.esprit.pidev.fdz.administrator.util.delegates;

import java.util.List;

import tn.esprit.pidev.fdz.administrator.util.ServiceLocator;
import tn.esprit.pidev.fdz.entities.Transaction;
import tn.esprit.pidev.fdz.services.interfaces.TransactionCrudRemote;

public class TransactionDelegate {
	
private static TransactionCrudRemote proxy = (TransactionCrudRemote) ServiceLocator.getInstance().getProxy("/tn.esprit.pidev.fdz/TransactionCrud!tn.esprit.pidev.fdz.services.interfaces.TransactionCrudRemote");
	
	
	
	public static void create(Transaction transaction)
	{
		proxy.create(transaction);
	}
	public static void edit(Transaction transaction)
	{
		proxy.edit(transaction);
	}
	public static void remove(Transaction transaction)
	{
		proxy.remove(transaction);
	}
	public static Transaction find(int id)
	{
		return proxy.find(id);
	}
	public static List<Transaction> findAll()
	{
		return proxy.findAll();
	}
	public static int count()
	{
		return proxy.count();
	}


}
