package tn.esprit.pidev.fdz.administrator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JTable;

import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;



import tn.esprit.pidev.fdz.administrator.util.delegates.UserDelegate;
import tn.esprit.pidev.fdz.entities.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelRecherche extends JPanel {
	//déclaration des variables
	List<User> userslist=new ArrayList<>();
	List<User> userslist2=new ArrayList<>();
	User user = new User();
	private JTextField textField;
	private JTable table;


	/**
	 * Create the panel.
	 */
	public PanelRecherche(List<User> users) {
		userslist=users;
		
		JScrollPane scrollPane = new JScrollPane();
		
		
		textField = new JTextField();
		textField.setColumns(10);
		
		final JButton btnNewButton = new JButton("Show List");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String l= 	textField.getText();
//				System.out.println(l);
				
//				for(User u :users)
//				{System.out.println(u.getLogin());}
				if(l.equalsIgnoreCase(""))
				{
					
					
					
					initDataBindings(userslist);
					System.out.println(table.getValueAt(0, 0));
					//add(table);
				}
				else {
					userslist2=UserDelegate.findByLogin(l);
					
					initDataBindings(userslist2);
					System.out.println(table.getValueAt(0, 0));
					//add(table);
					
				}
				
			}
		});
		
		final JButton btnLockunlock = new JButton("Lock/Unlock");
		btnLockunlock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int x =table.getSelectedRow();
				if(x==-1)
				{JOptionPane.showMessageDialog(null, "you must select a row");}
				else{
				//user=users.get(x);
				
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!"+x);
				 
				if (user.getStatus().equalsIgnoreCase("unlocked")) 
					
					
				 
			    {
					user.setStatus("locked");
				    UserDelegate.edit(user);
				    if(textField.getText().equalsIgnoreCase("")){
				    initDataBindings(userslist);
				    }
				    else{
				    	initDataBindings(userslist2);
				    }
			
				}
				else if(user.getStatus().equalsIgnoreCase("locked"))
				{
					user.setStatus("unlocked");
					UserDelegate.edit(user);
					if(textField.getText().equalsIgnoreCase("")){
					    initDataBindings(userslist);
					    }
					    else{
					    	initDataBindings(userslist2);
					    }
				}
				
				
			//users=UserDelegate.findAll();
			//initDataBindings();
				//clear();
			
			}
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(22)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 413, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(174)
							.addComponent(btnLockunlock)))
					.addContainerGap(15, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
					.addComponent(btnLockunlock)
					.addGap(21))
		);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String l= 	textField.getText();
				if(l.equalsIgnoreCase("")){
					user=userslist.get(table.getSelectedRow());
					if(user.getStatus()=="locked")
					{btnLockunlock.setText("Unlock");}
					else{btnLockunlock.setText("lock");}
				}
				
				else{
					user=userslist2.get(table.getSelectedRow());
					if(user.getStatus()=="locked")
					{btnLockunlock.setText("Unlock");}
					else{btnLockunlock.setText("lock");}
					
				}
			}
		});
		
		scrollPane.setViewportView(table);
		setLayout(groupLayout);

	}
	protected void initDataBindings(List<User> list) {
		JTableBinding<User, List<User>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, list, table);
		//
		BeanProperty<User, String> UserBeanProperty = BeanProperty.create("firstName");
		jTableBinding.addColumnBinding(UserBeanProperty).setColumnName("firstName");
		//
		BeanProperty<User, String> UserBeanProperty_1 = BeanProperty.create("lastName");
		jTableBinding.addColumnBinding(UserBeanProperty_1).setColumnName("lastName");
		//
		BeanProperty<User, String> UserBeanProperty_2 = BeanProperty.create("login");
		jTableBinding.addColumnBinding(UserBeanProperty_2).setColumnName("login");
		//
		BeanProperty<User, String> UserBeanProperty_3 = BeanProperty.create("status");
		jTableBinding.addColumnBinding(UserBeanProperty_3).setColumnName("status");
		//
		BeanProperty<User, String> UserBeanProperty_4 = BeanProperty.create("city");
		jTableBinding.addColumnBinding(UserBeanProperty_4).setColumnName("city");
		//
		BeanProperty<User, String> UserBeanProperty_5 = BeanProperty.create("country");
		jTableBinding.addColumnBinding(UserBeanProperty_5).setColumnName("country");
		//
		BeanProperty<User, String> UserBeanProperty_6 = BeanProperty.create("email");
		jTableBinding.addColumnBinding(UserBeanProperty_6).setColumnName("email");
		
		jTableBinding.bind();
	}
}
