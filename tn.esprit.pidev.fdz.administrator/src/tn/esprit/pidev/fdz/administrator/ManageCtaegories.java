package tn.esprit.pidev.fdz.administrator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;


import tn.esprit.pidev.fdz.administrator.util.delegates.CategoryDelegate;
//import test2.delegate.CategoryDelegate;
import tn.esprit.pidev.fdz.entities.Category;

import javax.swing.JTextField;

public class ManageCtaegories extends JPanel {
	private JTable table2;
	private JTable table_1;
	List<Category> categories = new ArrayList<Category>();
	List<Category> Sub_categories = new ArrayList<Category>();
	Category category = new Category();
	private JTextField textField;
    
	/**
	 * Create the panel.
	 */
	public ManageCtaegories(List<Category>list) {
		categories=list;
		
		JScrollPane scrollPane1 = new JScrollPane();
		
		JScrollPane scrollPane2 = new JScrollPane();
		
		JButton edit = new JButton("edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				Category cat = new Category();
		if((table2.getSelectedRow()>=0) &&(table_1.getSelectedRow()==-1)){			
					cat=categories.get(table2.getSelectedRow());
				
					//textField.setText(cat.getName());
					
//					
//					String name=textField.getText();
//					
//					System.out.println(name);
//					
//
					cat.setName(textField.getText());
					CategoryDelegate.edit(cat);
					categories=CategoryDelegate.ListCategory();
					initDataBindings(categories, table2);
//					
//					
//						
					
			//}
		}
//				 if ((table_1.getSelectedRow()>=0) &&( (table2.getSelectedRow()>=0))){7
		Category category1 = new Category();
		cat=Sub_categories.get(table_1.getSelectedRow());
		category1=categories.get(table2.getSelectedRow());

		cat.setName(textField.getText());
		CategoryDelegate.edit(cat);
		//categories=CategoryDelegate.ListCategory();
		//initDataBindings(categories, table2);
					
//					Category subcategory1 = new Category();
//					category=categories.get(table2.getSelectedRow());
//					subcategory1=Sub_categories.get(table_1.getSelectedRow());
//					
//					
//					CategoryDelegate.remove(subcategory1);
//					JOptionPane.showMessageDialog(null,  "Sub_caategorie deleted with succes");
					Sub_categories=CategoryDelegate.ListSubCategory(category1);
//					List<Category> newsub=CategoryDelegate.ListSubCategory(category);
//					
					initDataBindings(Sub_categories, table_1);
//					
			}	 
		});
		
		JButton delete = new JButton("delete");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((table2.getSelectedRow()==-1) && ((table_1.getSelectedRow()==-1)))
				{
					JOptionPane.showMessageDialog(null, "you must select a row");
				}
				if((table2.getSelectedRow()>=0) &&(table_1.getSelectedRow()==-1)){
				
					Category cat = new Category();
					cat=categories.get(table2.getSelectedRow());
					
					
					List<Category> list =CategoryDelegate.ListSubCategory(cat);
					for (Category category : list) {
						CategoryDelegate.remove(category);
						
						
					}
					
					CategoryDelegate.remove(cat);
					JOptionPane.showMessageDialog(null, "Category deleted with succes");
					categories=CategoryDelegate.ListCategory();
					initDataBindings(categories, table2);
					Sub_categories=CategoryDelegate.ListSubCategory(cat);
					initDataBindings(Sub_categories, table_1);
				}
				 if ((table_1.getSelectedRow()>=0) &&( (table2.getSelectedRow()>=0))){
					Category category1 = new Category();
					Category subcategory1 = new Category();
					category=categories.get(table2.getSelectedRow());
					subcategory1=Sub_categories.get(table_1.getSelectedRow());
					
					
					CategoryDelegate.remove(subcategory1);
					JOptionPane.showMessageDialog(null,  "Sub_caategorie deleted with succes");
					Sub_categories=CategoryDelegate.ListSubCategory(category1);
					List<Category> newsub=CategoryDelegate.ListSubCategory(category);
					
					initDataBindings(newsub, table_1);
					
				}
			}
		});
		
		JButton add = new JButton("add");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if((table2.getSelectedRow()==-1) && ((table_1.getSelectedRow()==-1)))
				{
					JOptionPane.showMessageDialog(null, "you must select a row");
				}
				if((table2.getSelectedRow()>=0) &&(table_1.getSelectedRow()==-1)){
					String name=textField.getText();
					Category cat = new Category();
					cat.setName(name);
					CategoryDelegate.create(cat);
					JOptionPane.showMessageDialog(null, "ajout effectu�");
					categories=CategoryDelegate.ListCategory();
					initDataBindings(categories, table2);
				}
				 if ((table_1.getSelectedRow()>=0) &&( (table2.getSelectedRow()>=0))){
					Category category1 = new Category();
					category1=categories.get(table2.getSelectedRow());
					String name=textField.getText();
					Category cat = new Category();
					cat.setName(name);
					cat.setIdCategory(category1);
					CategoryDelegate.create(cat);
					JOptionPane.showMessageDialog(null, "ajout effectu�");
					Sub_categories=CategoryDelegate.ListSubCategory(category1);
					initDataBindings(Sub_categories, table_1);
				}
				 
				
			}
		});
		
		JLabel lblListCategories = new JLabel("List Categories");
		
		JLabel lblListSubcategory = new JLabel("List Sub_Categories");
		
		JButton btnShowSubcategories = new JButton("Show SubCategories");
		btnShowSubcategories.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("bla bla!");
				Sub_categories=CategoryDelegate.ListSubCategory(category);
				initDataBindings(Sub_categories, table_1);
			}
		});
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblCategoryName = new JLabel("Category name");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(45)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(delete)
										.addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
									.addGap(27)
									.addComponent(btnShowSubcategories))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(96)
									.addComponent(lblListCategories)))
							.addGap(31)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblListSubcategory)
								.addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(108)
							.addComponent(lblCategoryName))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(166)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(add)
							.addGap(18)
							.addComponent(edit)))
					.addContainerGap(14, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblListCategories)
						.addComponent(lblListSubcategory))
					.addGap(31)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE, false)
						.addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(delete)
								.addComponent(btnShowSubcategories))
							.addGap(18)
							.addComponent(lblCategoryName)
							.addGap(15)))
					.addGap(1)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(add)
						.addComponent(edit))
					.addGap(136))
		);
		
		table_1 = new JTable();
		scrollPane2.setViewportView(table_1);
		scrollPane1.setViewportView(table2);
		table2 = new JTable();
		table2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
					category=categories.get(table2.getSelectedRow());
					
				
				
				
			}
		});
		scrollPane1.setViewportView(table2);
		initDataBindings(categories,table2);
		setLayout(groupLayout);

	}
	protected void initDataBindings(List<Category> list,JTable table) {
		JTableBinding<Category, List<Category>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, list, table);
		//
		BeanProperty<Category, String> CategoryBeanProperty = BeanProperty.create("name");
		jTableBinding.addColumnBinding(CategoryBeanProperty).setColumnName("name");
		//
		
		
		jTableBinding.bind();
	}
}

